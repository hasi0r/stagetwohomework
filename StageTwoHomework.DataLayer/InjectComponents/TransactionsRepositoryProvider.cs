﻿using System.Configuration;
using Ninject.Activation;
using StageTwoHomework.DataLayer.Repositories;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.DataLayer.InjectComponents
{
    public class TransactionsRepositoryProvider : Provider<ITransactionsRepository>
    {
        private readonly IUserRepository _userRepository;
        public TransactionsRepositoryProvider(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        protected override ITransactionsRepository CreateInstance(IContext context)
        {
            ITransactionsRepository transactionsRepository;
            var transactionsRepositoryConfig = ConfigurationManager.AppSettings["TransactionRepositorySource"];
            switch (transactionsRepositoryConfig)
            {
                case "TransactionsRepositoryMssql":
                    transactionsRepository = new TransactionsRepositoryMssql();
                    break;
                case "TransactionsRepositoryAzure":
                    var connectionString = ConfigurationManager.AppSettings["CurrencyAppAzureTransactionsDataConnectionString"];
                    transactionsRepository = new TransactionsRepositoryAzure("Transactions", connectionString, _userRepository);
                    break;
                default:
                    transactionsRepository = new TransactionsRepositoryMssql();
                    break;
            }
            return transactionsRepository;
        }
    }
}
