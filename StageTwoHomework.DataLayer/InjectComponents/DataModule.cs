﻿using Ninject.Modules;
using StageTwoHomework.DataLayer.Repositories;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.DataLayer.InjectComponents
{
    public class DataModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserRepository>().To<UserRepository>();
            Bind<ITransactionsRepository>().ToProvider<TransactionsRepositoryProvider>();
        }
    }
}
