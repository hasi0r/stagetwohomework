﻿using System;

namespace StageTwoHomework.DataLayer.Models
{
    public enum TransactionType { Withdraw, Deposit, Transaction }
    public class Transaction
    {
        public int Id { get; set; }
        public User User { get; set; }
        public string CurrencyCode { get; set; }
        public double? Quantity { get; set; }
        public double CashAmount { get; set; }
        public double? ExchangeRate { get; set; }
        public DateTime Date { get; set; }
        public TransactionType TransactionType { get; set; }
    }
}
