﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace StageTwoHomework.DataLayer.Models.AzureTableModels
{
    public class TransactionCloud : TableEntity
    {
        public Guid Id
        {
            get => new Guid(RowKey);
            set => RowKey = value.ToString();
        }

        public int UserId
        {
            get => int.Parse(PartitionKey);
            set => PartitionKey = value.ToString();
        }

        public string CurrencyCode { get; set; }
        public double? Quantity { get; set; }
        public double CashAmount { get; set; }
        public double? ExchangeRate { get; set; }
        public DateTime Date { get; set; }

        private TransactionType _transactionType;

        public TransactionType TransactionType
        {
            get => _transactionType;
            set => _transactionType = Enum.IsDefined(typeof(TransactionType), value) ? value : throw new Exception("Wrong enum value.");
        }

        public int TransactionTypeInt
        {
            get => (int)_transactionType;
            set => _transactionType = Enum.IsDefined(typeof(TransactionType), value) ? (TransactionType)value : throw new Exception("Wrong enum value.");
        }

    }
}
