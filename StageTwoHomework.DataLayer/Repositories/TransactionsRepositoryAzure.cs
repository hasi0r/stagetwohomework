﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using StageTwoHomework.DataLayer.Models;
using StageTwoHomework.DataLayer.Models.AzureTableModels;
using StageTwoHomework.DataLayer.Repositories.BaseRepositories;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.DataLayer.Repositories
{
    public class TransactionsRepositoryAzure : BaseTableStorageRepository, ITransactionsRepository
    {
        private readonly IUserRepository _userRepository;
        public TransactionsRepositoryAzure(string tableName, string connectionString, IUserRepository userRepository) : base(tableName, connectionString)
        {
            _userRepository = userRepository;
        }

        public void AddTransaction(Transaction transaction)
        {
            var newTransaction = new TransactionCloud
            {
                Id = Guid.NewGuid(),
                ExchangeRate = transaction.ExchangeRate,
                CashAmount = transaction.CashAmount,
                TransactionType = transaction.TransactionType,
                CurrencyCode = transaction.CurrencyCode,
                Date = transaction.Date,
                Quantity = transaction.Quantity,
                UserId = transaction.User.Id
            };
            var operation = TableOperation.Insert(newTransaction);
            TableReference.Execute(operation);
        }

        public List<Transaction> AllTransactionsForUser(int userId)
        {
            var transactions = new List<Transaction>();
            var cloudTransactions = TableReference.CreateQuery<TransactionCloud>().Where(t => t.UserId == userId).ToList();

            for (var i = 0; i < cloudTransactions.Count; i++)
            {
                transactions.Add(new Transaction
                {
                    Id = i + 1,
                    CashAmount = cloudTransactions[i].CashAmount,
                    ExchangeRate = cloudTransactions[i].ExchangeRate,
                    User = _userRepository.GetUserById(cloudTransactions[i].UserId),
                    Quantity = cloudTransactions[i].Quantity,
                    TransactionType = cloudTransactions[i].TransactionType,
                    Date = cloudTransactions[i].Date,
                    CurrencyCode = cloudTransactions[i].CurrencyCode
                });
            }
            return transactions;
        }

        public double GetCashAmountForUser(int userId)
        {
            var cashAmount = TableReference.CreateQuery<TransactionCloud>()
                .Where(t => t.UserId == userId)
                .Select(t => t.CashAmount)
                .ToList()
                .DefaultIfEmpty()
                .Sum();

            return cashAmount;
        }

        public double GetUsersOwnedCurrencyAmount(int userId, string currencyCode)
        {
            var cashAmount = TableReference.CreateQuery<TransactionCloud>()
                .Where(t => t.UserId == userId && t.CurrencyCode == currencyCode)
                .Select(t => t.Quantity)
                .ToList()
                .DefaultIfEmpty()
                .Sum();

            return cashAmount ?? 0.0;
        }
    }
}
