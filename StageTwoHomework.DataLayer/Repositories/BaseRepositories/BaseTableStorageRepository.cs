﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace StageTwoHomework.DataLayer.Repositories.BaseRepositories
{
    public class BaseTableStorageRepository
    {
        protected readonly CloudTable TableReference;

        public BaseTableStorageRepository(string tableName, string connectionString)
        {
           var cloudStorageAccount = CloudStorageAccount.Parse(connectionString);

            var tableClient = cloudStorageAccount.CreateCloudTableClient();

            TableReference = tableClient.GetTableReference(tableName);

            TableReference.CreateIfNotExists();
        }
    }
}
