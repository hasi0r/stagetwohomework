﻿using System.Collections.Generic;
using StageTwoHomework.DataLayer.Models;

namespace StageTwoHomework.DataLayer.Repositories.Interfaces
{
    public interface ITransactionsRepository
    {
        void AddTransaction(Transaction transaction);
        List<Transaction> AllTransactionsForUser(int userId);
        double GetCashAmountForUser(int userId);
        double GetUsersOwnedCurrencyAmount(int userId, string currencyCode);
    }
}