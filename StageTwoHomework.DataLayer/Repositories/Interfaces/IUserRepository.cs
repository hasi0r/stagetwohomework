﻿using StageTwoHomework.DataLayer.Models;

namespace StageTwoHomework.DataLayer.Repositories.Interfaces
{
    public interface IUserRepository
    {
        User GetUserByLogin(string login);
        User GetUserById(int userId);
        int RegiesterUser(User user);
    }
}