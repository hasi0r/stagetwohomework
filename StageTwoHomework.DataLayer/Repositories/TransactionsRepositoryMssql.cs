﻿using System.Collections.Generic;
using StageTwoHomework.DataLayer.Data;
using StageTwoHomework.DataLayer.Models;
using System.Linq;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.DataLayer.Repositories
{
    public class TransactionsRepositoryMssql : ITransactionsRepository
    {
        public void AddTransaction(Transaction transaction)
        {
            using (var dbContext = new CryptoCurrenciesDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(u => u.Id == transaction.User.Id);
                transaction.User = user;

                dbContext.Transactions.Add(transaction);
                dbContext.SaveChanges();
            }
        }

        public List<Transaction> AllTransactionsForUser(int userId)
        {
            using (var dbContext = new CryptoCurrenciesDbContext())
            {
                var test = dbContext.Transactions.Where(t => t.User.Id == userId).ToList();
                return test;
            }
        }

        public double GetCashAmountForUser(int userId)
        {
            using (var dbContext = new CryptoCurrenciesDbContext())
            {
                var cashAmount = dbContext.Transactions
                    .Where(t => t.User.Id == userId)
                    .Select(t => t.CashAmount)
                    .DefaultIfEmpty()
                    .Sum();

                return cashAmount;
            }
        }

        public double GetUsersOwnedCurrencyAmount(int userId, string currencyCode)
        {
            using (var dbContext = new CryptoCurrenciesDbContext())
            {
                var cashAmount = dbContext.Transactions
                    .Where(t => t.User.Id == userId && t.CurrencyCode == currencyCode)
                    .Select(t => t.Quantity)
                    .DefaultIfEmpty()
                    .Sum();

                return cashAmount ?? 0.0;
            }
        }
    }
}
