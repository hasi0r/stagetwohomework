﻿using System.Linq;
using StageTwoHomework.DataLayer.Data;
using StageTwoHomework.DataLayer.Models;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.DataLayer.Repositories
{
    public class UserRepository : IUserRepository
    {
        public User GetUserByLogin(string login)
        {
            using (var dbContext = new CryptoCurrenciesDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(x => x.Login == login);
                return user;
            }
        }

        public User GetUserById(int userId)
        {
            using (var dbContext = new CryptoCurrenciesDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(x => x.Id == userId);
                return user;
            }
        }

        public int RegiesterUser(User user)
        {
            using (var dbContext = new CryptoCurrenciesDbContext())
            {
                var existingUser = dbContext.Users.SingleOrDefault(x => x.Login == user.Login);
                if (existingUser != null)
                {
                    return 0;
                }

                var registeredUser = dbContext.Users.Add(user);
                dbContext.SaveChanges();

                return registeredUser.Id;
            }
        }
        
    }
}
