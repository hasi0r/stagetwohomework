﻿using System.Configuration;
using System.Data.Entity;
using StageTwoHomework.DataLayer.Models;

namespace StageTwoHomework.DataLayer.Data
{
    internal class CryptoCurrenciesDbContext : DbContext
    {
        public CryptoCurrenciesDbContext() : base(GetConnectionString())
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        
        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["CurrenciesDb"].ConnectionString;
        }
    }
}
