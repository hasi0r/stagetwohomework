import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {

  messages: string[] = [];
  constructor() { }

  clearMessage() {
    this.messages = [];
  }
  addMessage(message: string) {
    this.messages.push(message);
  }
}
