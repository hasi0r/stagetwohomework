import { AuthenticationService } from './security/authentication/authentication.service';
import { UserService } from './security/user.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { LoginComponent } from './security/login/login.component';
import { NavComponent } from './nav/nav.component';
import {AppRoutingModule} from './app-routing.module';
import { MessageComponent } from './message/message/message.component';
import {MessageService} from './message/message.service';
import { TransactionsListComponent } from './transactions/transactions-list/transactions-list.component';
import {TransactionService} from './transactions/transaction.service';
import {Ng2OrderModule} from 'ng2-order-pipe';
import {CurrencyCodePipe} from './transactions/currencyCode.pipe';
import {Angular2FontawesomeModule} from 'angular2-fontawesome';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavComponent,
    MessageComponent,
    TransactionsListComponent,
    CurrencyCodePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    Ng2OrderModule,
    Angular2FontawesomeModule
  ],
  providers: [
    UserService,
    AuthenticationService,
    MessageService,
    TransactionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
