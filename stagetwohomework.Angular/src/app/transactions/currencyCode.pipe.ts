import { Pipe, PipeTransform } from '@angular/core';
@Pipe({ name: 'currencyCode' })

export class CurrencyCodePipe implements PipeTransform {
  transform(codes: any, searchText: any): any {
    if (searchText == null) {
      return codes;
    }

    return codes.filter(function (currencyCode) {
      return currencyCode.CurrencyCode.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
    });
  }
}

