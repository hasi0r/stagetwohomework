import { Injectable } from '@angular/core';
import {User} from '../security/user';
import {Observable} from 'rxjs/Observable';
import {Transaction} from './transaction';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {TransactionType} from './transactionType';
import {MessageService} from '../message/message.service';

@Injectable()
export class TransactionService {


  constructor(private http: HttpClient,
              private messageService: MessageService) { }

  getTransactions(user: User): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(environment.currencyApi + 'users/' + user.id + '/transactions');
  }


  getTransactionTypesFromWebApi(): Observable<TransactionType[]> {
    return this.http.get<TransactionType[]>(environment.currencyApi + 'transactions/types');
  }

}

