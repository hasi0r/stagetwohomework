///<reference path="../currencyCode.pipe.ts"/>
import {Component, OnInit} from '@angular/core';
import {Transaction} from '../transaction';
import {TransactionService} from '../transaction.service';
import {MessageService} from '../../message/message.service';
import {UserService} from '../../security/user.service';
import {TransactionType} from '../transactionType';
@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.css']
})
export class TransactionsListComponent implements OnInit {

  transactions: Transaction[] = [];
  types: TransactionType[] = [];
  searchText = '';
  sortType = 'Date';
  sortReverse  = false;

  constructor(private transactionService: TransactionService,
              private messageService: MessageService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.transactionService.getTransactionTypesFromWebApi()
      .subscribe(data => {
        this.types = data;
        this.getTransactions();
        },
        error => this.messageService.addMessage('Transaction types are not aviable') );
  }

  getTransactions() {
    this.transactionService
      .getTransactions(this.userService.getCurrentUser())
      .subscribe(data => {
        data.forEach((item, index) => {
            item.TransactionType = this.getTransactionTypeDescriptionById(item.TransactionType);
            this.transactions = data;
          },
          error => this.messageService.addMessage('Transactions not aviable'));
      });
  }
  getTransactionTypeDescriptionById( typeId: string): string {
    return this.types.find(x => x.Value === typeId).Name;
  }

  changeSortType(sort: string) {
    this.sortType = sort;
    this.sortReverse = !this.sortReverse;
  }
}
