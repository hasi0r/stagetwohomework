export class Transaction {

  Id: number;
  UserId: number;
  CurrencyCode: string;
  Quantity: number;
  CashAmount: number;
  ExchangeRate: number;
  Date: Date;
  TransactionType: string;
}
