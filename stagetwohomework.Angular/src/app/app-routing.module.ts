import { LoginComponent } from './security/login/login.component';
import { AuthGuard } from './security/authorization/auth.guard';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TransactionsListComponent} from './transactions/transactions-list/transactions-list.component';

const routes: Routes = [
  { path: '', canActivate: [AuthGuard], canActivateChild: [AuthGuard], children: [
      {path: '', redirectTo: 'transactions', pathMatch: 'full'},
      { path: 'transactions', component: TransactionsListComponent },
    ]},
  {path: 'login', component: LoginComponent },
  {path: '**', redirectTo: '/transactions'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
