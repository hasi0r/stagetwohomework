import { AuthenticationService } from './../security/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import {UserService} from '../security/user.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private authService: AuthenticationService,
              private userService: UserService) { }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }
}
