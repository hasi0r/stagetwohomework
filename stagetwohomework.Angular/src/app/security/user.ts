export class User {
    constructor(public userName: string,
        public id: number,
        public isAuthenticated: boolean,
        public isAuthorized: boolean) {}
}
