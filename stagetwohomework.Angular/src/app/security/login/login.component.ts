import { UserCredentials } from '../userCredentials';
import { UserService } from '../user.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MessageService} from '../../message/message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userCredentials: UserCredentials = new UserCredentials();

  constructor(private authService: AuthenticationService,
              private userService: UserService,
              private router: Router,
              private messageService: MessageService) { }

  ngOnInit() {
  }

  login() {
   this.authService.login(this.userCredentials)
   .subscribe(user => {
      if (user.isAuthenticated) {
        this.userService.setCurrentUser(user);
        this.router.navigateByUrl('/transactions');
      } else {
        this.messageService.addMessage('User: ' + user.userName + ' is not authenticated.');
      }
      this.userCredentials = new UserCredentials();
    },
       error => this.messageService.addMessage('Server is not answearing') );
  }
}
