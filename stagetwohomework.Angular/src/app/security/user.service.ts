import { Injectable } from '@angular/core';
import { User } from './user';

@Injectable()
export class UserService {

  constructor() { }


  setCurrentUser(user: User) {
    const userJson = JSON.stringify(user);
    localStorage.setItem('login', userJson);
  }

  getCurrentUser(): User {
    const currentUserJson = localStorage.getItem('login');
    if (currentUserJson) {
      return JSON.parse(currentUserJson) as User;
    }
    return null;
  }

  removeUser() {
    localStorage.removeItem('login');
  }

}
