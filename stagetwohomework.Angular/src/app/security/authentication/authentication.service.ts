import { UserService } from '../user.service';
import { User } from '../user';
import { Injectable } from '@angular/core';
import { UserCredentials } from '../userCredentials';
import { Observable } from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient,
              private userService: UserService,
              private router: Router) { }


  login(credentials: UserCredentials): Observable<User> {
    return this.http
      .post<any>(environment.currencyApi + 'authentication', credentials)
      .pipe(
        map(response => {
          const user = new User(response.Login, response.Id, response.IsAutheticated, response.IsAutheticated);
          return user;
        })
      );
  }

  logout() {
    this.userService.removeUser();
    this.router.navigateByUrl('/login');
  }
}
