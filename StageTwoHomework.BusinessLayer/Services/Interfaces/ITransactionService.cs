﻿using StageTwoHomework.BusinessLayer.BusinessModels;

namespace StageTwoHomework.BusinessLayer.Services.Interfaces
{
    public interface ITransactionService
    {
        void DepositFunds(ExternalTransaction transferData);
        bool WithdrawFunds(ExternalTransaction transferData);
        bool BuyCurrency(TransactionInputData newTransactionInput);
        bool SellCurrency(TransactionInputData newTransactionInput);
    }
}