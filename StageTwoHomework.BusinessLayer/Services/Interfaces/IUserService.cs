﻿using System.Collections.Generic;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;

namespace StageTwoHomework.BusinessLayer.Services.Interfaces
{
    public interface IUserService
    {
        UserDto GetUserByLogin(string login);
        int RegisterUser(UserDto user);
        List<TransactionDto> GetUserTransactionHistory(int userId);
        double GetUserFunds(int userId);
        double GetUsersCurrencyAmount(int userId, string currencyCode);
    }
}