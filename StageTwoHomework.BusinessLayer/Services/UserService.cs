﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.DataLayer.Models;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.BusinessLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ITransactionsRepository _transactionsRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper, ITransactionsRepository transactionsRepository)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _transactionsRepository = transactionsRepository;
        }

        public UserDto GetUserByLogin(string login)
        {
            var user = _userRepository.GetUserByLogin(login);
            return _mapper.Map<User, UserDto>(user);
        }

        public int RegisterUser(UserDto user)
        {
            return user == null ? 0 : _userRepository.RegiesterUser(_mapper.Map<UserDto, User>(user));
        }

        public List<TransactionDto> GetUserTransactionHistory(int userId)
        {
            return _transactionsRepository.AllTransactionsForUser(userId)
                .Select(transaction => _mapper.Map<Transaction, TransactionDto>(transaction))
                .ToList();
        }

        public double GetUserFunds(int userId)
        {
            return _transactionsRepository.GetCashAmountForUser(userId);
        }

        public double GetUsersCurrencyAmount(int userId, string currencyCode)
        {
            return _transactionsRepository.GetUsersOwnedCurrencyAmount(userId, currencyCode);
        }
    }
}
