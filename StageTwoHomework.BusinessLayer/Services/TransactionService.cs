﻿using AutoMapper;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.DataLayer.Models;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.BusinessLayer.Services
{
    public class TransactionService : ITransactionService
    {
        private const int NegativeMultiplier = -1;
        private readonly ITransactionsRepository _transactionsRepository;
        private readonly IMapper _mapper;

        public TransactionService(ITransactionsRepository transactionsRepository, IMapper mapper)
        {
            _transactionsRepository = transactionsRepository;
            _mapper = mapper;
        }

        public void DepositFunds(ExternalTransaction transferData)
        {
            var transaction = new Transaction
            {
                User = _mapper.Map<UserDto, User>(transferData.User),
                ExchangeRate = null,
                CurrencyCode = transferData.CurrencyCode,
                Quantity = null,
                CashAmount = transferData.CashAmount,
                Date = transferData.Date,
                TransactionType = TransactionType.Deposit
            };
            _transactionsRepository.AddTransaction(transaction);
        }

        public bool WithdrawFunds(ExternalTransaction transferData)
        {
            var userFunds = _transactionsRepository.GetCashAmountForUser(transferData.User.Id);

            if (userFunds < transferData.CashAmount)
            {
                return false;
            }

            var transaction = new Transaction
            {
                User = _mapper.Map<UserDto, User>(transferData.User),
                ExchangeRate = null,
                CurrencyCode = transferData.CurrencyCode,
                Quantity = null,
                CashAmount = transferData.CashAmount * NegativeMultiplier,
                Date = transferData.Date,
                TransactionType = TransactionType.Withdraw
            };
            _transactionsRepository.AddTransaction(transaction);
            return true;
        }

        public bool BuyCurrency(TransactionInputData newTransactionInputData)
        {
            var userFunds = _transactionsRepository.GetCashAmountForUser(newTransactionInputData.User.Id);

            var cashAmount = newTransactionInputData.Quantity * newTransactionInputData.ExchangeRate;
            if (cashAmount > userFunds)
            {
                return false;
            }

            var transaction = new Transaction
            {
                User = _mapper.Map<UserDto, User>(newTransactionInputData.User),
                CurrencyCode = newTransactionInputData.CurrencyCode,
                Date = newTransactionInputData.Date,
                Quantity = newTransactionInputData.Quantity,
                CashAmount = cashAmount * NegativeMultiplier,
                ExchangeRate = newTransactionInputData.ExchangeRate,
                TransactionType = TransactionType.Transaction
            };
            _transactionsRepository.AddTransaction(transaction);
            return true;
        }
        public bool SellCurrency(TransactionInputData newTransactionInputData)
        {
            var ownedQuantity = _transactionsRepository.GetUsersOwnedCurrencyAmount(newTransactionInputData.User.Id, newTransactionInputData.CurrencyCode);
            if (ownedQuantity < newTransactionInputData.Quantity)
            {
                return false;
            }

            var cashAmount = newTransactionInputData.Quantity * newTransactionInputData.ExchangeRate;
            var transaction = new Transaction
            {
                User = _mapper.Map<UserDto, User>(newTransactionInputData.User),
                CurrencyCode = newTransactionInputData.CurrencyCode,
                Date = newTransactionInputData.Date,
                Quantity = newTransactionInputData.Quantity * NegativeMultiplier,
                CashAmount = cashAmount,
                ExchangeRate = newTransactionInputData.ExchangeRate,
                TransactionType = TransactionType.Transaction
            };
            _transactionsRepository.AddTransaction(transaction);
            return true;
        }
    }
}
