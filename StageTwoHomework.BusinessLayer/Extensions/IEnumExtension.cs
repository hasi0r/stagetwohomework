﻿using System.Collections.Generic;
using StageTwoHomework.BusinessLayer.BusinessModels;

namespace StageTwoHomework.BusinessLayer.Extensions
{
    public interface IEnumExtension
    {
        List<EnumValue> GetValues<T>();
    }
}
