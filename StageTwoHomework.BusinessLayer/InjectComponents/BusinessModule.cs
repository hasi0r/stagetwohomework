﻿using AutoMapper;
using Ninject.Modules;
using StageTwoHomework.BusinessLayer.CurrencyProviders.Interfaces;
using StageTwoHomework.BusinessLayer.Extensions;
using StageTwoHomework.BusinessLayer.Services;
using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.DataLayer.InjectComponents;

namespace StageTwoHomework.BusinessLayer.InjectComponents
{
    public class BusinessModule : NinjectModule
    {
        public override void Load()
        {
            Kernel?.Load(new[] {new DataModule()});
            Bind<IUserService>().To<UserService>();
            Bind<IMapper>().ToProvider<MapperProvider>();
            Bind<IEnumExtension>().To<EnumExtension>();
            Bind<IExchangeRateSupplier>().ToProvider<ExchangeRateSupplierProvider>().InSingletonScope();
        }
    }
}
