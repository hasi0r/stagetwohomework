﻿using AutoMapper;
using Ninject.Activation;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.DataLayer.Models;

namespace StageTwoHomework.BusinessLayer.InjectComponents
{
    internal class MapperProvider : Provider<IMapper>
    {
        protected override IMapper CreateInstance(IContext context)
        {
            var mapperConfiguration = new MapperConfiguration(config =>
            {
                config.CreateMap<User, UserDto>();
                config.CreateMap<UserDto, User>();
                config.CreateMap<Transaction, TransactionDto>();
            });

            return new Mapper(mapperConfiguration);
        }
    }
}
