﻿using System.Configuration;
using Ninject.Activation;
using StageTwoHomework.BusinessLayer.CurrencyProviders;
using StageTwoHomework.BusinessLayer.CurrencyProviders.Interfaces;

namespace StageTwoHomework.BusinessLayer.InjectComponents
{
    public class ExchangeRateSupplierProvider : Provider<IExchangeRateSupplier>
    {
        protected override IExchangeRateSupplier CreateInstance(IContext context)
        {
            IExchangeRateSupplier exchangeRateSupplier;

            var supplierFromConfig = ConfigurationManager.AppSettings["ExchangeRateSupplier"];
            switch (supplierFromConfig)
            {
                case "CurrencyExchangeRateWebApiClient":
                    exchangeRateSupplier = new CurrencyExchangeRateWebApiClient();
                    break;
                case "ExchangeRateGenerator":
                    exchangeRateSupplier = new ExchangeRateGenerator();
                    break;
                default:
                    exchangeRateSupplier = new ExchangeRateGenerator();
                    break;
            }
            return exchangeRateSupplier;
        }
    }
}
