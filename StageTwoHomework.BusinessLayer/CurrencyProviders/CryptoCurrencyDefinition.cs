﻿namespace StageTwoHomework.BusinessLayer.CurrencyProviders
{
    public class CurrencyDefinition
    {
        public string Name { get; set; }
        public int MinRate { get; set; }
        public int MaxRate { get; set; }
        public string Code { get; set; }
    }
}