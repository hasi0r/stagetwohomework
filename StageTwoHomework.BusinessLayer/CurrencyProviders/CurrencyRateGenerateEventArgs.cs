﻿using System;
using System.Collections.Generic;
using StageTwoHomework.BusinessLayer.BusinessModels;

namespace StageTwoHomework.BusinessLayer.CurrencyProviders
{
    public class CurrencyRateGenerateEventArgs : EventArgs
    {
        public List<Currency> Currencies;
    }
}