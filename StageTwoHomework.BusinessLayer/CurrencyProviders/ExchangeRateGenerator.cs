﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Threading;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.CurrencyProviders.Interfaces;

namespace StageTwoHomework.BusinessLayer.CurrencyProviders
{
    public class ExchangeRateGenerator : IExchangeRateSupplier
    {
        private const double MinRateMultiplier = 0.9;
        private const double MaxRateMultiplier = 1.1;

        private List<CurrencyDefinition> _currenciesDefinitionList;
        private readonly List<Currency> _actualCurrenciesList;
        private readonly Random _random;
        public event GeneratedCurrenciesHandler OnNewCurrienciesProvided;

        public ExchangeRateGenerator()
        {

            _random = new Random(DateTime.Now.Millisecond);
            _actualCurrenciesList = new List<Currency>();
        }

        public void StartProvider(IEnumerable<CurrencyDefinition> currenciesDeffinitions)
        {
            if (currenciesDeffinitions == null)
            {
                return;
            }
            _currenciesDefinitionList = currenciesDeffinitions.ToList();

            if (_actualCurrenciesList.Count == 0)
            {
                foreach (var currency in _currenciesDefinitionList)
                {
                    if (_actualCurrenciesList.FindIndex(x => x.Code == currency.Code) >= 0)
                    {
                        continue;
                    }
                    _actualCurrenciesList.Add(new Currency { ExchangeRate = 0.0d, Name = currency.Name, Code = currency.Code });
                }
            }

            var bgWorker = new BackgroundWorker();
            bgWorker.DoWork += GenerateExchangeRates;
            bgWorker.RunWorkerAsync();
        }

        private void GenerateExchangeRates(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (_currenciesDefinitionList == null)
                {
                    break;
                }
                var args = new CurrencyRateGenerateEventArgs { Currencies = new List<Currency>() };

                foreach (var currency in _actualCurrenciesList)
                {
                    var currencyDefinition = _currenciesDefinitionList.SingleOrDefault(x => x.Code == currency.Code);
                    if (currencyDefinition == null)
                    {
                        break;
                    }
                    if (Math.Abs(currency.ExchangeRate) < double.Epsilon)
                    {
                        currency.ExchangeRate = _random.NextDouble() * (currencyDefinition.MaxRate - currencyDefinition.MinRate) + currencyDefinition.MinRate;
                    }
                    else
                    {
                        var min = Convert.ToInt32(currency.ExchangeRate * MinRateMultiplier);
                        min = min < currencyDefinition.MinRate ? currencyDefinition.MinRate : min;

                        var max = Convert.ToInt32(currency.ExchangeRate * MaxRateMultiplier);
                        max = max > currencyDefinition.MaxRate ? currencyDefinition.MaxRate : max;

                        currency.ExchangeRate = _random.NextDouble() * (max - min) + min;
                    }

                    args.Currencies.Add(new Currency
                    {
                        Name = currency.Name,
                        Code = currency.Code,
                        ExchangeRate = currency.ExchangeRate
                    });
                }

                var refreshTime = int.Parse(ConfigurationManager.AppSettings["RefreshTime"]);
                Thread.Sleep(refreshTime);
                OnNewCurrienciesProvided?.Invoke(this, args);
            }
        }
    }
}