﻿using System.Collections.Generic;

namespace StageTwoHomework.BusinessLayer.CurrencyProviders.Interfaces
{
    public delegate void GeneratedCurrenciesHandler(object sender, CurrencyRateGenerateEventArgs e);

    public interface IExchangeRateSupplier
    {
        event GeneratedCurrenciesHandler OnNewCurrienciesProvided;
        void StartProvider(IEnumerable<CurrencyDefinition> currenciesDeffinitions);
    }
}