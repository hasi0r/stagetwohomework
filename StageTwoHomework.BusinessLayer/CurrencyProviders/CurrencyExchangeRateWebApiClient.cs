﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading;
using Newtonsoft.Json;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.CurrencyProviders.Interfaces;

namespace StageTwoHomework.BusinessLayer.CurrencyProviders
{
    public class CurrencyExchangeRateWebApiClient : IExchangeRateSupplier
    {
        public event GeneratedCurrenciesHandler OnNewCurrienciesProvided;
        private string _apiAdress;

        private List<CurrencyDefinition> _currenciesDefinitionList;

        public void StartProvider(IEnumerable<CurrencyDefinition> currenciesDeffinitions)
        {
            if (currenciesDeffinitions == null)
            {
                return;
            }
            _currenciesDefinitionList = currenciesDeffinitions.ToList();

            _apiAdress = ConfigurationManager.AppSettings["WebApiUri"];

            var bgWorker = new BackgroundWorker();
            bgWorker.DoWork += GetRatesFromWebApi;
            bgWorker.RunWorkerAsync();
        }

        public void GetRatesFromWebApi(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                var httpClinet = new HttpClient();
                var args = new CurrencyRateGenerateEventArgs { Currencies = new List<Currency>() };

                foreach (var currencyDefinition in _currenciesDefinitionList)
                {
                    var response = httpClinet.GetAsync(_apiAdress + "/" + currencyDefinition.Code + "PLN/ticker.json").Result;
                    var actualCurrencyRate = JsonConvert.DeserializeObject<CurrencyJson>(response.Content.ReadAsStringAsync().Result);
                    args.Currencies.Add(new Currency
                    {
                        Name = currencyDefinition.Name,
                        Code = currencyDefinition.Code,
                        ExchangeRate = actualCurrencyRate.last
                    });
                }

                OnNewCurrienciesProvided?.Invoke(this, args);
                var refreshTime = int.Parse(ConfigurationManager.AppSettings["RefreshTime"]);
                Thread.Sleep(refreshTime);
            }
        }
    }
}
