﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;

namespace StageTwoHomework.BusinessLayer.DataExport
{
    public class JsonFileService : IDataSerializer
    {
        public void Export(string fileName, List<TransactionDto> transactions)
        {
            var output = JsonConvert.SerializeObject(transactions
                .Select(t => new
                {
                    t.Id,
                    t.ExchangeRate,
                    t.CashAmount,
                    t.CurrencyCode,
                    t.Date,
                    t.Quantity,
                    t.TransactionType,
                    UserId = t.User.Id
                }),
                Formatting.Indented);
            
            File.WriteAllText(fileName, output);
        }
    }
}
