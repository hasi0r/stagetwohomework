﻿using System.Collections.Generic;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;

namespace StageTwoHomework.BusinessLayer.DataExport
{
    public interface IDataSerializer
    {
        void Export(string fileName, List<TransactionDto> transactions);
    }
}