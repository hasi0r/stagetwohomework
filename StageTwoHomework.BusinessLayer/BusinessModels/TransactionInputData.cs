﻿using System;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;

namespace StageTwoHomework.BusinessLayer.BusinessModels
{
    public class TransactionInputData
    {
        public UserDto User { get; set; }
        public string CurrencyCode { get; set; }
        public double Quantity { get; set; }
        public double ExchangeRate { get; set; }
        public DateTime Date { get; set; }
    }
}
