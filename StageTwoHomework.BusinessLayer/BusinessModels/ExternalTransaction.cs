﻿using System;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;

namespace StageTwoHomework.BusinessLayer.BusinessModels
{
    public class ExternalTransaction
    {
        public UserDto User { get; set; }
        public string CurrencyCode { get; set; }
        public double CashAmount { get; set; }
        public DateTime Date { get; set; }
    }
}
