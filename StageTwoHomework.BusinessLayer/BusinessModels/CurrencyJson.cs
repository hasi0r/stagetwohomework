﻿namespace StageTwoHomework.BusinessLayer.BusinessModels
{
    public class CurrencyJson
    {
        public double max { get; set; }
        public double min { get; set; }
        public double last { get; set; }
        public double bid { get; set; }
        public double ask { get; set; }
        public double vwap { get; set; }
        public double average { get; set; }
        public double volume { get; set; }
    }
}
