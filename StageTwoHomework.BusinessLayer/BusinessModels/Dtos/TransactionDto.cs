﻿using System;

namespace StageTwoHomework.BusinessLayer.BusinessModels.Dtos
{
    public enum TransactionDtoType { Withdraw, Deposit, Transaction }

    public class TransactionDto
    {
        public int Id { get; set; }
        public UserDto User { get; set; }
        public string CurrencyCode { get; set; }
        public double? Quantity { get; set; }
        public double CashAmount { get; set; }
        public double? ExchangeRate { get; set; }
        public DateTime Date { get; set; }
        public TransactionDtoType TransactionType { get; set; }
    }
}
