﻿namespace StageTwoHomework.BusinessLayer.BusinessModels
{
    public class EnumValue
    {
        public string Name { get; set; }
        public int Value { get; set; }      
    }
}
