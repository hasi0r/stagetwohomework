﻿namespace StageTwoHomework.BusinessLayer.BusinessModels
{
    public class Currency
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public double ExchangeRate { get; set; }
    }
}
