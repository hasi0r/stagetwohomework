﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.WebApi.Models;
using StageTwoHomework.WebApi.Services;

namespace StageTwo.homework.WebApi.Tests
{
    [TestClass]
    public class AuthenticationServiceTests
    {
        [TestMethod]
        public void AuthenticateUser_ValidCredentials_ValidResult()
        {
            var credentials = new AuthenticationCredentials
            {
                Login = "TestUser",
                Password = "TestPassword"
            };

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetUserByLogin(credentials.Login))
                .Returns(new UserDto
                {
                    Id = 1,
                    Login = "TestUser",
                    Password = "TestPassword",
                    Name = "Test"
                });
            var authenticationService = new AuthenticationService(userServiceMock.Object);

            //Act

            var result = authenticationService.AutheticateUser(credentials);

            //Assert

            Assert.AreEqual(1, result.Id);
            Assert.AreEqual( true,result.IsAutheticated);
            Assert.AreEqual(credentials.Login, result.Login);
        }

        [TestMethod]
        public void AuthenticateUser_InvalidPassword_FalseResult()
        {
            var credentials = new AuthenticationCredentials
            {
                Login = "TestUser",
                Password = "TestPassword"
            };

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetUserByLogin(credentials.Login))
                .Returns(new UserDto
                {
                    Id = 1,
                    Login = "TestUser",
                    Password = "DifferentPassowrd",
                    Name = "Test"
                });
            var authenticationService = new AuthenticationService(userServiceMock.Object);

            //Act

            var result = authenticationService.AutheticateUser(credentials);

            //Assert

            Assert.AreEqual(0, result.Id);
            Assert.AreEqual(false, result.IsAutheticated);
            Assert.AreEqual(credentials.Login, result.Login);

        }
    }
}
