﻿using Ninject;
using StageTwoHomework.Cli.InjectComponents;

namespace StageTwoHomework.Cli
{
    internal class Program
    {
        private static void Main()
        {
            IKernel kernel = new StandardKernel(new CliModule());
            var programLoop = kernel.Get<ProgramLoop>();

            programLoop.Run();
        }
    }
}
