﻿using Ninject.Modules;
using StageTwoHomework.BusinessLayer.DataExport;
using StageTwoHomework.BusinessLayer.InjectComponents;
using StageTwoHomework.BusinessLayer.Services;
using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.Cli.ConsoleHelpers;
using StageTwoHomework.Cli.ConsoleHelpers.Interfaces;
using StageTwoHomework.Cli.Menus;
using StageTwoHomework.Cli.Menus.Interfaces;
using StageTwoHomework.Cli.Menus.MenuComponents;
using StageTwoHomework.Cli.Menus.SessionData;

namespace StageTwoHomework.Cli.InjectComponents
{
    public class CliModule : NinjectModule
    {
        public override void Load()
        {
            Kernel?.Load(new[] { new BusinessModule() });
            Bind<IConsoleHelper>().To<ConsoleHelper>();
            Bind<IDataReader>().To<DataReader>();
            Bind<IMenu>().To<Menu>();
            Bind<IProgramMenu>().To<LoginMenu>().Named("Login");
            Bind<IProgramMenu>().To<MainMenu>().Named("Inner");
            Bind<IMainMenuOptions>().To<MainMenuOptions>();
            Bind<ISession>().To<Session>().InSingletonScope();
            Bind<ITransactionService>().To<TransactionService>();
            Bind<IDataSerializer>().To<JsonFileService>();
        }
    }
}
