﻿using System.Collections.Generic;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;

namespace StageTwoHomework.Cli.Menus.SessionData
{
    public interface ISession
    {
        UserDto WorkingUser { get; set; }
        List<Currency> ActualCurrencies { get; set; }
    }
}