﻿using System.Collections.Generic;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;

namespace StageTwoHomework.Cli.Menus.SessionData
{
    internal class Session : ISession
    {
        public UserDto WorkingUser { get; set; }
        public List<Currency> ActualCurrencies { get; set; }
    }  
}
