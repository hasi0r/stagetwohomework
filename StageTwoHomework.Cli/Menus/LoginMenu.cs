﻿using Ninject;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.Cli.ConsoleHelpers.Interfaces;
using StageTwoHomework.Cli.Menus.Interfaces;
using StageTwoHomework.Cli.Menus.MenuComponents;
using StageTwoHomework.Cli.Menus.SessionData;

namespace StageTwoHomework.Cli.Menus
{
    internal class LoginMenu : IProgramMenu
    {
        private readonly IConsoleHelper _consoleHelper;
        private readonly IMenu _menu;
        private readonly IDataReader _dataReader;
        private readonly IUserService _userService;
        private readonly IProgramMenu _innerMenu;
        private readonly ISession _session;
        private bool _exit;

        public LoginMenu(IConsoleHelper consoleHelper,IMenu menu, IDataReader dataReader, IUserService userService,[Named("Inner")] IProgramMenu innerMenu, ISession session)
        {
            _consoleHelper = consoleHelper;
            _menu = menu;
            _dataReader = dataReader;
            _userService = userService;
            _innerMenu = innerMenu;
            _session = session;
            _exit = false;
        }

        public void InitializeMenu()
        {
            while (!_exit)
            {
                _consoleHelper.ClearConsole();
                AddMainLoginCommands();
                _menu.PrintMenuOptions();

                var choice = _dataReader.ReadIntNumber("choose");
                _menu.InvokeCommand(choice);
                _menu.ClearOptions();
                if (_session.WorkingUser != null)
                {
                    _innerMenu.InitializeMenu();
                }
                _session.WorkingUser = null;
            }
        }

        private void AddMainLoginCommands()
        {
            _menu.AddOption(new MenuOption("Login", Login));
            _menu.AddOption(new MenuOption("Register", Register));
            _menu.AddOption(new MenuOption("Exit", Exit));
            _menu.MenuHeader = "Welcome to currencies app.";
        }
        private void Register()
        {
            var login = _dataReader.ReadString("Type your login");
            var existingUser = _userService.GetUserByLogin(login);

            while (existingUser != null)
            {
                login = _dataReader.ReadString("Login already exists, try again");
                existingUser = _userService.GetUserByLogin(login);
            }

            var newUser = new UserDto
            {
                Login = login,
                Password = _dataReader.ReadString("Type your Password"),
                Name = _dataReader.ReadString("Type your Name")
            };
            _userService.RegisterUser(newUser);
        }

        private void Login()
        {
            var login = _dataReader.ReadString("Login");
            var password = _dataReader.ReadString("Password");
            var user = _userService.GetUserByLogin(login);
          
            if (user != null && user.Password == password )
            {
                _session.WorkingUser = user;
                _consoleHelper.ShowMessage("Login succesfull.");
                _consoleHelper.WaitForEnter();
                _consoleHelper.ClearConsole();
                return;
            }

            _consoleHelper.ShowMessage("Incorrect login or password.");
            _consoleHelper.WaitForEnter();
            _consoleHelper.ClearConsole();
        }

        private void Exit()
        {
            _exit = true;
        }
    }
}
