﻿namespace StageTwoHomework.Cli.Menus.MenuComponents
{
    public interface IMenu
    {
        bool Exit { get; }
        string ExitCommand { get; set; }
        string MenuHeader { get; set; }

        void AddOption(MenuOption option);
        void InvokeCommand(int commandNumber);
        void PrintMenuOptions();
        void ClearOptions();
    }
}