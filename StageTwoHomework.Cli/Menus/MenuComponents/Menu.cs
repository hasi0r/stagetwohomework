﻿using System.Collections.Generic;
using StageTwoHomework.Cli.ConsoleHelpers.Interfaces;

namespace StageTwoHomework.Cli.Menus.MenuComponents
{
    internal class Menu : IMenu
    {
        private List<MenuOption> _options = new List<MenuOption>();
        private readonly IConsoleHelper _consoleHelper;

        public Menu(IConsoleHelper consoleHelper)
        {
            _consoleHelper = consoleHelper;
        }

        public bool Exit { get; private set; }
        public string ExitCommand { get; set; }
        public string MenuHeader { get; set; }

        public void AddOption(MenuOption option)
        {
            _options.Add(option);
        }

        public void ClearOptions()
        {
           _options = new List<MenuOption>();
        }
        public void PrintMenuOptions()
        {
            _consoleHelper.ShowMessage(MenuHeader);
            int i;

            for (i = 0; i < _options.Count; i++)
            {
                _consoleHelper.ShowMessage($"{i+1} - {_options[i].Command}");
            }
            if (!string.IsNullOrWhiteSpace(ExitCommand))
            {
                _consoleHelper.ShowMessage($"{_options.Count+1} - {ExitCommand}");
            }
        }

        public void InvokeCommand(int commandNumber)
        {
            if (commandNumber == _options.Count + 1)
            {
                Exit = true;
                return;
            }

            if (commandNumber > _options.Count)
            {
                _consoleHelper.ShowMessage($"Command with number {commandNumber} is undefined");
                _consoleHelper.WaitForEnter();
            }
            else
            {
                var menuOption = _options[commandNumber - 1];
                menuOption.CallbackHandler();
            }
        }
    }
}
