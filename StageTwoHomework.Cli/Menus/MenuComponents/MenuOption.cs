﻿namespace StageTwoHomework.Cli.Menus.MenuComponents
{
    public class MenuOption
    {
        public delegate void CommandCallback();

        public object Command { get; }
        public CommandCallback CallbackHandler { get; }

        public MenuOption(object command, CommandCallback callbackHandler)
        {
            Command = command;
            CallbackHandler = callbackHandler;
        }
    }
}