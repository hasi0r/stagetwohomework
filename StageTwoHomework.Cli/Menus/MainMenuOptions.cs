﻿using System;
using System.IO;
using System.Linq;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.BusinessLayer.CurrencyProviders;
using StageTwoHomework.BusinessLayer.CurrencyProviders.Interfaces;
using StageTwoHomework.BusinessLayer.DataExport;
using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.Cli.ConsoleHelpers.Interfaces;
using StageTwoHomework.Cli.Menus.Interfaces;
using StageTwoHomework.Cli.Menus.SessionData;

namespace StageTwoHomework.Cli.Menus
{
    public class MainMenuOptions : IMainMenuOptions
    {
        private readonly IConsoleHelper _consoleHelper;
        private readonly IDataReader _dataReader;
        private readonly ISession _session;
        private readonly IExchangeRateSupplier _exchangeRateSupplier;
        private readonly ITransactionService _transactionService;
        private readonly IUserService _userService;
        private readonly IDataSerializer _dataSerializer;

        public MainMenuOptions(IConsoleHelper consoleHelper, IDataReader dataReader, ISession session, IExchangeRateSupplier exchangeRateSupplier, ITransactionService transactionService, IUserService userService, IDataSerializer dataSerializer)
        {
            _consoleHelper = consoleHelper;
            _dataReader = dataReader;
            _session = session;
            _exchangeRateSupplier = exchangeRateSupplier;
            _transactionService = transactionService;
            _userService = userService;
            _dataSerializer = dataSerializer;
        }

        public void PrintTransactionHistory()
        {
            var transactions = _userService.GetUserTransactionHistory(_session.WorkingUser.Id);
            if (transactions.Count == 0)
            {
                _consoleHelper.ShowMessage("There is no transactions");
                _consoleHelper.WaitForEnter();
                return;
            }
            transactions = transactions.OrderBy(t => t.Date).ToList();

            _consoleHelper.ClearConsole();
            _consoleHelper.ShowMessage("Completed transactions:");
            for (var i = 0; i < transactions.Count; i++)
            {
                if (transactions[i].TransactionType == TransactionDtoType.Transaction)
                {
                    _consoleHelper.ChangeFontColor(ConsoleColor.DarkGreen);
                    _consoleHelper.ShowMessage(
                        $"{i + 1} - {transactions[i].CurrencyCode}, Date: {transactions[i].Date} quantity: {transactions[i].Quantity}, " +
                        $"exchange rate: {transactions[i].ExchangeRate}, PLN value: {transactions[i].CashAmount}, {transactions[i].TransactionType.ToString()}");
                }
                else
                {
                    _consoleHelper.ChangeFontColor(ConsoleColor.Gray);
                    _consoleHelper.ShowMessage(
                        $"{i + 1} - {transactions[i].CurrencyCode}, Date: {transactions[i].Date}, PLN value: {transactions[i].CashAmount}, {transactions[i].TransactionType.ToString()}");
                }
            }
            _consoleHelper.ChangeFontColor(ConsoleColor.Gray);
            _consoleHelper.WaitForEnter();
        }

        public void SellCurrency()
        {
            _consoleHelper.ClearConsole();

            PrintOwnedCurrencies();
            _consoleHelper.ShowMessage("Do you want sell?(Y to continue)");
            if (_consoleHelper.ReadKeyPressed() != ConsoleKey.Y)
            {
                return;
            }
            _consoleHelper.ShowMessage("");

            var choice = _dataReader.ReadIntNumber(1, _session.ActualCurrencies.Count, "Choose currency to sell");
            InvokeSellTransaction(choice - 1);
            _consoleHelper.WaitForEnter();
        }

        private void InvokeSellTransaction(int choice)
        {
            var ownedQuantity =
                _userService.GetUsersCurrencyAmount(_session.WorkingUser.Id,
                    _session.ActualCurrencies[choice].Code);
            if (ownedQuantity <= 0)
            {
                _consoleHelper.ShowMessage("You dont have any amount of choosen currency");
                return;
            }
            var quantity = _dataReader.ReadDoubleNumber(0, ownedQuantity, "How much you want sell");
            var exchangeRate = _session.ActualCurrencies[choice].ExchangeRate;

            var newTransaction = new TransactionInputData
            {
                User = _session.WorkingUser,
                CurrencyCode = _session.ActualCurrencies[choice].Code,
                Date = DateTime.Now,
                Quantity = quantity,
                ExchangeRate = exchangeRate
            };
            if (_transactionService.SellCurrency(newTransaction))
            {
                _consoleHelper.ShowMessage(
                    $"Sold {newTransaction.Quantity} {newTransaction.CurrencyCode} with exchange rate: {exchangeRate} PLN/{newTransaction.CurrencyCode}.");
                return;
            }
            _consoleHelper.ShowMessage("Something went wrong");
        }

        private void PrintOwnedCurrencies()
        {
            _consoleHelper.ShowMessage($"Your funds:{_userService.GetUserFunds(_session.WorkingUser.Id)} PLN.");

            for (var i = 0; i < _session.ActualCurrencies.Count; i++)
            {
                var quantity = _userService.GetUsersCurrencyAmount(_session.WorkingUser.Id, _session.ActualCurrencies[i].Code);
                _consoleHelper.ShowMessage(
                    $"{i + 1}. {_session.ActualCurrencies[i].Code}, amount owned: {quantity}, exchange rate: {_session.ActualCurrencies[i].ExchangeRate}");
            }
        }

        public void BuyCurrency()
        {
            _consoleHelper.ClearConsole();
            PrintCurrenciesToBuy();
            _consoleHelper.ShowMessage("Do you want buy?(Y to continue)");
            if (_consoleHelper.ReadKeyPressed() != ConsoleKey.Y)
            {
                return;
            }
            _consoleHelper.ShowMessage("");

            var choice = _dataReader.ReadIntNumber(1, _session.ActualCurrencies.Count,
                "Choose currency to buy(exchange rate may change)");

            var isSuccess = InvokeBuyTransaction(choice - 1);

            _consoleHelper.ShowMessage(isSuccess ? "Transaction sucessful." : "Not enought funds.");
            _consoleHelper.WaitForEnter();
        }

        private void PrintCurrenciesToBuy()
        {
            _consoleHelper.ShowMessage(
                $"Your funds: {_userService.GetUserFunds(_session.WorkingUser.Id):0.00} zł");

            for (var i = 0; i < _session.ActualCurrencies.Count; i++)
            {
                _consoleHelper.ShowMessage(
                    $"{i + 1} - {_session.ActualCurrencies[i].Code}, exchange rate: {_session.ActualCurrencies[i].ExchangeRate:0.00} zł");
            }
        }

        private bool InvokeBuyTransaction(int choice)
        {
            var quantity = _dataReader.ReadDoubleNumber("How much you wanna buy", false);

            var newTransaction = new TransactionInputData
            {
                User = _session.WorkingUser,
                CurrencyCode = _session.ActualCurrencies[choice].Code,
                Date = DateTime.Now,
                Quantity = quantity,
                ExchangeRate = _session.ActualCurrencies[choice].ExchangeRate
            };

            var success = _transactionService.BuyCurrency(newTransaction);
            return success;
        }

        public void WithdrawFunds()
        {
            var funds = _userService.GetUserFunds(_session.WorkingUser.Id);

            _consoleHelper.ShowMessage($"You funds: {funds}");
            var outsideTransfer = new ExternalTransaction()
            {
                User = _session.WorkingUser,
                CurrencyCode = "PLN",
                CashAmount = Math.Abs(_dataReader.ReadDoubleNumber("Provide how much PLN you want withdraw", true)),
                Date = DateTime.Now
            };
            var result = _transactionService.WithdrawFunds(outsideTransfer);

            _consoleHelper.ShowMessage(result ? "Cash successfully withdraw." : "Not enought funds to withdraw that amount.");
            _consoleHelper.WaitForEnter();
        }

        public void ExportTransactionsToJson()
        {
            var path = _dataReader.ReadString("Provide path to file(with file name).");
            if (File.Exists(path))
            {
                _consoleHelper.ShowMessage("File already exists.\n" +
                                           "Do you want to overwrite file?(Y to continue)");
                if (_consoleHelper.ReadKeyPressed() != ConsoleKey.Y)
                {
                    return;
                }
                _consoleHelper.ShowMessage("");
            }

            var transactions = _userService.GetUserTransactionHistory(_session.WorkingUser.Id);

            try
            {
                _dataSerializer.Export(path, transactions);
            }
            catch (Exception e)
            {
                _consoleHelper.ShowMessage(e.Message);
                _consoleHelper.WaitForEnter();
                return;
            }

            _consoleHelper.ShowMessage("Success.");
            _consoleHelper.WaitForEnter();
        }

        public void DepositFunds()
        {
            var outsideTransfer = new ExternalTransaction
            {
                User = _session.WorkingUser,
                CurrencyCode = "PLN",
                CashAmount = Math.Abs(_dataReader.ReadDoubleNumber("Provide how much PLN you want deposit", true)),
                Date = DateTime.Now
            };
            _transactionService.DepositFunds(outsideTransfer);
        }

        public void Logout()
        {
            _session.WorkingUser = null;
        }

        public void PrintExchangeRates()
        {
            _consoleHelper.ClearConsole();
            _consoleHelper.ShowMessage("Exchange rates(press esc to go back):");
            _exchangeRateSupplier.OnNewCurrienciesProvided += PrintActualCurrencies;
            PrintActualCurrencies(null, null);
            do
            {
            } while (_consoleHelper.ReadKeyPressed() != ConsoleKey.Escape);

            _exchangeRateSupplier.OnNewCurrienciesProvided -= PrintActualCurrencies;
        }

        private void PrintActualCurrencies(object sender, CurrencyRateGenerateEventArgs e)
        {
            _consoleHelper.SetCoursorPosition(0, 1);
            foreach (var currency in _session.ActualCurrencies)
            {
                _consoleHelper.ShowMessage($"{currency.Name}({currency.Code}): {currency.ExchangeRate:0.00} zł");
            }
        }
    }
}
