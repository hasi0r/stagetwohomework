﻿namespace StageTwoHomework.Cli.Menus.Interfaces
{
    internal interface IProgramMenu
    {
        void InitializeMenu();
    }
}