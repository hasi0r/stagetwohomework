﻿namespace StageTwoHomework.Cli.Menus.Interfaces
{
    public interface IMainMenuOptions
    {
        void BuyCurrency();
        void DepositFunds();
        void Logout();
        void PrintExchangeRates();
        void PrintTransactionHistory();
        void SellCurrency();
        void WithdrawFunds();
        void ExportTransactionsToJson();
    }
}