﻿using StageTwoHomework.Cli.ConsoleHelpers.Interfaces;
using StageTwoHomework.Cli.Menus.Interfaces;
using StageTwoHomework.Cli.Menus.MenuComponents;
using StageTwoHomework.Cli.Menus.SessionData;

namespace StageTwoHomework.Cli.Menus
{
    public class MainMenu : IProgramMenu
    {
        private readonly IMenu _menu;
        private readonly IConsoleHelper _consoleHelper;
        private readonly IDataReader _dataReader;
        private readonly ISession _session;
        private readonly IMainMenuOptions _mainMenuOptions;

        public MainMenu(IMenu menu, IConsoleHelper consoleHelper, IDataReader dataReader, ISession session, IMainMenuOptions mainMenuOptions)
        {
            _menu = menu;
            _consoleHelper = consoleHelper;
            _dataReader = dataReader;
            _session = session;
            _mainMenuOptions = mainMenuOptions;
        }

        public void InitializeMenu()
        {
            while (_session.WorkingUser != null)
            {
                _consoleHelper.ClearConsole();

                AddMainMenuCommands();
                _menu.PrintMenuOptions();

                var choice = _dataReader.ReadIntNumber("Choose");
                _menu.InvokeCommand(choice);
                _menu.ClearOptions();
            }
        }

        private void AddMainMenuCommands()
        {
            _menu.AddOption(new MenuOption("Check exchangerates", _mainMenuOptions.PrintExchangeRates));
            _menu.AddOption(new MenuOption("Deposit funds.", _mainMenuOptions.DepositFunds));
            _menu.AddOption(new MenuOption("Withdraw funds.", _mainMenuOptions.WithdrawFunds));
            _menu.AddOption(new MenuOption("Buy cryptocurency.", _mainMenuOptions.BuyCurrency));
            _menu.AddOption(new MenuOption("Sell cryptocurency.", _mainMenuOptions.SellCurrency));
            _menu.AddOption(new MenuOption("Transaction history.", _mainMenuOptions.PrintTransactionHistory));
            _menu.AddOption(new MenuOption("Export transaction history to Json file.", _mainMenuOptions.ExportTransactionsToJson));
            _menu.AddOption(new MenuOption("Logout", _mainMenuOptions.Logout));
            _menu.MenuHeader = $"Welcome {_session.WorkingUser.Login}. What you want to do?";
        }

    }
}