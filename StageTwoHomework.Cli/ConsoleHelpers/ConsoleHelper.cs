﻿using System;
using StageTwoHomework.Cli.ConsoleHelpers.Interfaces;

namespace StageTwoHomework.Cli.ConsoleHelpers
{
    internal class ConsoleHelper : IConsoleHelper
    {
        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
        public void ClearConsole()
        {
            Console.Clear();
        }

        public void SetCoursorPosition(int left, int right)
        {
            Console.SetCursorPosition(left, right);
        }

        public void WaitForEnter()
        {
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();
        }

        public ConsoleKey ReadKeyPressed()
        {
            return Console.ReadKey().Key;
        }

        public void ChangeFontColor(ConsoleColor color)
        {
            Console.ForegroundColor = color;
        }
    }
}
