﻿using System;
using System.Globalization;
using StageTwoHomework.Cli.ConsoleHelpers.Interfaces;

namespace StageTwoHomework.Cli.ConsoleHelpers
{
    internal class DataReader : IDataReader
    {
        public int ReadIntNumber(string message)
        {
            Console.Write($"- {message}: ");
            var input = Console.ReadLine();
            int result;
            while (!(int.TryParse(input, out result)))
            {
                Console.Write("Worng data, provide number: ");
                input = Console.ReadLine();
            }
            return result;
        }
        public int ReadIntNumber(int min, int max, string message)
        {
            Console.Write($"- {message}: ");
            string input = Console.ReadLine();
            int result;
            while (!(int.TryParse(input, out result)) || result < min || result > max)
            {
                Console.Write($"Provide number higher than {min} and lower than {max}: ");
                input = Console.ReadLine();
            }
            return result;
        }
        public bool ReadBoolean(string message)
        {
            var input = ReadIntNumber(0, 1, message).ToString();

            if (input == "1")
            {
                input = "true";
            }
            else
            {
                input = "false";
            }
            bool result = Convert.ToBoolean(input);
            return result;
        }
        public double ReadDoubleNumber(double min, double max, string message)
        {
            Console.Write($"- {message}: ");
            var input = Console.ReadLine();
            double result;
            while (!(double.TryParse(input, out result)) || result < min || result > max)
            {
                Console.Write($"Provide number higher than {min} and lower than {max}: ");
                input = Console.ReadLine();
            }
            return result;
        }
        public double ReadDoubleNumber(string message, bool onlyPositive)
        {
            Console.Write($"- {message}: ");
            var input = Console.ReadLine();
            double result;
            if (onlyPositive)
            {
                while (!(double.TryParse(input, out result)) || result < 0)
                {
                    Console.Write($"Worng data, provide positive number: ");
                    input = Console.ReadLine();
                }
            }
            else
            {
                while (!(double.TryParse(input, out result)))
                {
                    Console.Write("Worng data, provide number: ");
                    input = Console.ReadLine();
                }
            }
            return result;
        }

        public DateTime ReadDateTime(string message)
        {
            var format = "dd.MM.yyyy";
            Console.Write($"- {message}({format}): ");
            var input = Console.ReadLine();
            DateTime result;
            while (!(DateTime.TryParseExact(input, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out result)))
            {
                Console.Write($"Provide correct format of date({format}): ");
                input = Console.ReadLine();
            }
            return result;
        }
        public string ReadString(string message)
        {
            Console.Write($"- {message}: ");
            var input = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(input))
            {
                Console.Write("Empty text is not accepted.Try again.:");
                input = Console.ReadLine();
            }
            return input;
        }
    }
}