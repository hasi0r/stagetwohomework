﻿using System;

namespace StageTwoHomework.Cli.ConsoleHelpers.Interfaces
{
    public interface IDataReader
    {
        bool ReadBoolean(string message);
        DateTime ReadDateTime(string message);
        double ReadDoubleNumber(double min, double max, string message);
        double ReadDoubleNumber(string message, bool onlyPositive);
        int ReadIntNumber(int min, int max, string message);
        int ReadIntNumber(string message);
        string ReadString(string message);
    }
}