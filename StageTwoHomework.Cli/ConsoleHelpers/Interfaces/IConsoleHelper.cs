﻿using System;

namespace StageTwoHomework.Cli.ConsoleHelpers.Interfaces
{
    public interface IConsoleHelper
    {
        void ShowMessage(string message);
        void WaitForEnter();
        void ClearConsole();
        void SetCoursorPosition(int left, int right);
        ConsoleKey ReadKeyPressed();
        void ChangeFontColor(ConsoleColor color);
    }
}