﻿using System.Collections.Generic;
using System.Linq;
using Ninject;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.CurrencyProviders;
using StageTwoHomework.BusinessLayer.CurrencyProviders.Interfaces;
using StageTwoHomework.Cli.Menus.Interfaces;
using StageTwoHomework.Cli.Menus.SessionData;

namespace StageTwoHomework.Cli
{
    internal class ProgramLoop
    {
        private readonly IProgramMenu _programMenu;
        private readonly IExchangeRateSupplier _exchangeRateSupplier;
        private readonly ISession _session;

        public ProgramLoop([Named("Login")]IProgramMenu programMenu, IExchangeRateSupplier exchangeRateSupplier, ISession session)
        {
            _programMenu = programMenu;
            _exchangeRateSupplier = exchangeRateSupplier;
            _session = session;
            RunCurriencesGenerator();
            _exchangeRateSupplier.OnNewCurrienciesProvided += GetActualCurrenciesRates;
        }

        public void Run()
        {
            _programMenu.InitializeMenu();
        }
    
        private void RunCurriencesGenerator()
        {
            var currencies = SetupCurriencies();
            _exchangeRateSupplier.StartProvider(currencies);
        }

        private IEnumerable<CurrencyDefinition> SetupCurriencies()
        {
            var btc = new CurrencyDefinition
            {
                Name = "Bitcoin",
                Code = "BTC",
                MaxRate = 40000,
                MinRate = 10000
            };
            var bcc = new CurrencyDefinition
            {
                Name = "BitcoinCash",
                Code = "BCC",
                MaxRate = 10000,
                MinRate = 2000
            };
            var eth = new CurrencyDefinition
            {
                Name = "Ethereum",
                Code = "ETH",
                MaxRate = 2400,
                MinRate = 1800
            };
            var ltc = new CurrencyDefinition
            {
                Name = "Litecoin",
                Code = "LTC",
                MaxRate = 400,
                MinRate = 100
            };
            return new List<CurrencyDefinition> { btc, bcc, eth, ltc };
        }

        private void GetActualCurrenciesRates(object sender, CurrencyRateGenerateEventArgs e)
        {
            if (_session.ActualCurrencies == null)
            {
                _session.ActualCurrencies = new List<Currency>();
            }
            if (_session.ActualCurrencies.Count == 0)
            {
                foreach (var currency in e.Currencies)
                {
                    _session.ActualCurrencies.Add(new Currency
                    {
                        Name = currency.Name,
                        Code = currency.Code,
                        ExchangeRate = currency.ExchangeRate
                    });
                }
                return;
            }
            foreach (var currency in e.Currencies)
            {
                var existingCurrency = _session.ActualCurrencies.First(c => c.Code == currency.Code);
                var index = _session.ActualCurrencies.IndexOf(existingCurrency);
                existingCurrency.ExchangeRate = currency.ExchangeRate;

                if (index != -1)
                    _session.ActualCurrencies[index] = existingCurrency;
            }
        }
    }
}
