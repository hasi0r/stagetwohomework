﻿using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

namespace StageTwoHomework.WebApi
{
    internal class StartUp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();

            SwaggerConfig.Register(config);

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            // config.DependencyResolver = new NinjectResolver(NinjectBootstrap.GetKernel());

            config.EnableCors(new EnableCorsAttribute("*", "*", "*", "X-Custom-Header"));
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "CurrenciesApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            appBuilder.UseNinjectMiddleware(NinjectBootstrap.GetKernel).UseNinjectWebApi(config);
        }
    }
}
