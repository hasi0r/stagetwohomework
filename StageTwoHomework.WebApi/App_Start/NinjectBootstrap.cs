﻿using Ninject;
using StageTwoHomework.BusinessLayer.InjectComponents;
using StageTwoHomework.WebApi.Services;

namespace StageTwoHomework.WebApi
{
    internal static class NinjectBootstrap
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel(new BusinessModule());
            kernel.Bind<IAuthenticationService>().To<AuthenticationService>();

            return kernel;
        }
    }
}
