﻿using System;
using System.Configuration;
using System.Net.Http;
using Microsoft.Owin.Hosting;

namespace StageTwoHomework.WebApi
{
    internal class Program
    {
        private static void Main()
        {
            var apiAdress = ConfigurationManager.AppSettings["ApiAdress"];

            using (WebApp.Start<StartUp>(apiAdress))
            {
                var client = new HttpClient();
                var response = client.GetAsync(apiAdress + "api/status").Result;

                Console.WriteLine("StatusCode:" + response.StatusCode);
                Console.ReadLine();
            }
        }
    }
}
