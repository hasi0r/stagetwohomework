﻿using StageTwoHomework.WebApi.Models;

namespace StageTwoHomework.WebApi.Services
{
    public interface IAuthenticationService
    {
        AuthenticationResult AutheticateUser(AuthenticationCredentials credentials);
    }
}