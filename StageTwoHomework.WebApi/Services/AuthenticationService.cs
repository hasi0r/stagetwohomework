﻿using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.WebApi.Models;

namespace StageTwoHomework.WebApi.Services
{
    public class AuthenticationService : IAuthenticationService
    {

        private readonly IUserService _userService;

        public AuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        public AuthenticationResult AutheticateUser(AuthenticationCredentials credentials)
        {
            var user = _userService.GetUserByLogin(credentials.Login);

            if (user != null && user.Password == credentials.Password)
            {
                return new AuthenticationResult
                {
                    Login = user.Login,
                    Id = user.Id,
                    IsAutheticated = true
                };
            }

            return new AuthenticationResult
            {
                Login = credentials.Login,
                Id = 0,
                IsAutheticated = false
            };
        }
    }
}
