﻿using System;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;

namespace StageTwoHomework.WebApi.Models
{
    public class TransactionData
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CurrencyCode { get; set; }
        public double? Quantity { get; set; }
        public double CashAmount { get; set; }
        public double? ExchangeRate { get; set; }
        public DateTime Date { get; set; }
        public TransactionDtoType TransactionType { get; set; }
    }
}
