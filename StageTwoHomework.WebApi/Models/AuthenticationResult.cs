﻿namespace StageTwoHomework.WebApi.Models
{
    public class AuthenticationResult
    {
        public string Login { get; set; }
        public int Id { get; set; }
        public bool IsAutheticated { get; set; }
    }
}
