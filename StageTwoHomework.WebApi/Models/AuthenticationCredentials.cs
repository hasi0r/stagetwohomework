﻿namespace StageTwoHomework.WebApi.Models
{
    public class AuthenticationCredentials
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
