﻿using System.Web.Http;

namespace StageTwoHomework.WebApi.Controllers
{
    public class StatusController : ApiController
    {
        public string Get()
        {
            return "Ok";
        }
    }
}
