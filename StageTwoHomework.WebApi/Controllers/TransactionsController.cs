﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.BusinessLayer.Extensions;
using StageTwoHomework.BusinessLayer.Services.Interfaces;
using StageTwoHomework.WebApi.Models;

namespace StageTwoHomework.WebApi.Controllers
{
    public class TransactionsController : ApiController
    {
        private readonly IUserService _iUserService;
        private readonly IEnumExtension _enumExtension;

        public TransactionsController(IUserService iUserService, IEnumExtension enumExtension)
        {
            _iUserService = iUserService;
            _enumExtension = enumExtension;
        }

        [Route("api/users/{userId:int}/transactions")]
        [HttpGet]
        public IEnumerable<TransactionData> GetTransactionsByUserId(int userId)
        {
            return _iUserService.GetUserTransactionHistory(userId)
                .Select(t => new TransactionData
                {
                    Id = t.Id,
                    ExchangeRate = t.ExchangeRate,
                    CurrencyCode = t.CurrencyCode,
                    TransactionType = t.TransactionType,
                    Date = t.Date,
                    CashAmount = t.CashAmount,
                    Quantity = t.Quantity,
                    UserId = userId
                });
        }

        [Route("api/transactions/types")]
        [HttpGet]
        public IEnumerable<EnumValue> GetTransactionTypes()
        {
            return _enumExtension.GetValues<TransactionDtoType>();
        }
    }
}
