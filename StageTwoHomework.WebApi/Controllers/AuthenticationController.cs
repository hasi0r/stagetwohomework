﻿using System.Web.Http;
using StageTwoHomework.WebApi.Models;
using StageTwoHomework.WebApi.Services;

namespace StageTwoHomework.WebApi.Controllers
{
    public class AuthenticationController : ApiController
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost]
        public AuthenticationResult Authenticate(AuthenticationCredentials credentials)
        {
            return _authenticationService.AutheticateUser(credentials);
        }
    }
}
