﻿using System;
using AutoMapper;
using Moq;
using NUnit.Framework;
using StageTwoHomework.BusinessLayer.BusinessModels;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.BusinessLayer.Services;
using StageTwoHomework.DataLayer.Models;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.BusinessLayer.Tests
{
    [TestFixture]
    public class TransactionServiceTests
    {
        private readonly Mock<IUserRepository> _userRepositoryMock = new Mock<IUserRepository>();
        private readonly Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private readonly Mock<ITransactionsRepository> _transactionsRepositoryMock = new Mock<ITransactionsRepository>();

        [Test]
        public void WithdrawFunds_ValidData_Success()
        {
            //Arrange
            var date = DateTime.Now;

            var userDto = new UserDto
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transferData = new ExternalTransaction
            {
                User = userDto,
                CurrencyCode = "PLN",
                Date = date,
                CashAmount = 100

            };
            var user = new User
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transaction = new Transaction
            {
                User = user,
                ExchangeRate = null,
                CurrencyCode = "PLN",
                Quantity = null,
                CashAmount = -100,
                Date = date,
                TransactionType = TransactionType.Withdraw
            };
            _transactionsRepositoryMock.Setup(x => x.GetCashAmountForUser(1)).Returns(100);
            _transactionsRepositoryMock.Setup(x => x.AddTransaction(transaction));
            _mapperMock.Setup(x => x.Map<UserDto, User>(It.Is<UserDto>(u => u.Id == userDto.Id
                                                                            && u.Login == userDto.Login
                                                                            && u.Name == userDto.Name
                                                                            && u.Password == userDto.Password)))
                .Returns(user);

            var transactionService = new TransactionService(_transactionsRepositoryMock.Object, _mapperMock.Object);
            //Act
            var result = transactionService.WithdrawFunds(transferData);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void WithdrawFunds_NotEnoughtFunds_False()
        {
            //Arrange
            var date = DateTime.Now;

            var userDto = new UserDto
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transferData = new ExternalTransaction
            {
                User = userDto,
                CurrencyCode = "PLN",
                Date = date,
                CashAmount = 100

            };
            var user = new User
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transaction = new Transaction
            {
                User = user,
                ExchangeRate = null,
                CurrencyCode = "PLN",
                Quantity = null,
                CashAmount = -100,
                Date = date,
                TransactionType = TransactionType.Withdraw
            };
            _transactionsRepositoryMock.Setup(x => x.GetCashAmountForUser(1)).Returns(10);
            _transactionsRepositoryMock.Setup(x => x.AddTransaction(transaction));
            _mapperMock.Setup(x => x.Map<UserDto, User>(It.Is<UserDto>(u => u.Id == userDto.Id
                                                                            && u.Login == userDto.Login
                                                                            && u.Name == userDto.Name
                                                                            && u.Password == userDto.Password)))
                .Returns(user);

            var transactionService = new TransactionService(_transactionsRepositoryMock.Object, _mapperMock.Object);
            //Act
            var result = transactionService.WithdrawFunds(transferData);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void DepositFunds_IsAddTransactionCalledOnce()
        {
            //Arrange
            var date = DateTime.Now;

            var userDto = new UserDto
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transferData = new ExternalTransaction
            {
                User = userDto,
                CurrencyCode = "PLN",
                Date = date,
                CashAmount = 100
            };
            var user = new User
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transaction = new Transaction
            {
                Id = 0,
                User = user,
                ExchangeRate = null,
                CurrencyCode = "PLN",
                Quantity = null,
                CashAmount = 100,
                Date = date,
                TransactionType = TransactionType.Deposit
            };

            _mapperMock.Setup(x => x.Map<UserDto, User>(It.Is<UserDto>(u => u.Id == userDto.Id
                                                                            && u.Login == userDto.Login
                                                                            && u.Name == userDto.Name
                                                                            && u.Password == userDto.Password)))
                .Returns(user);

            var transactionService = new TransactionService(_transactionsRepositoryMock.Object, _mapperMock.Object);

            //Act
            transactionService.DepositFunds(transferData);

            //Assert
            _transactionsRepositoryMock.Verify(x => x.AddTransaction(It.Is<Transaction>(t => t.Id == transaction.Id
               && t.User == transaction.User
               && t.ExchangeRate == transaction.ExchangeRate
               && t.CurrencyCode == transaction.CurrencyCode
               && t.Quantity == transaction.Quantity
               && t.CashAmount == transaction.CashAmount
               && t.Date == transaction.Date
               && t.TransactionType == transaction.TransactionType)), Times.Once);
        }

        [Test]
        public void BuyCurrency_ValidData_Success()
        {
            //Arrange
            var date = DateTime.Now;

            var userDto = new UserDto
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transactionInputData = new TransactionInputData
            {
                User = userDto,
                CurrencyCode = "BTC",
                Date = date,
                Quantity = 1.5,
                ExchangeRate = 200
            };
            var user = new User
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transaction = new Transaction
            {
                User = user,
                ExchangeRate = 200,
                CurrencyCode = "BTC",
                Quantity = 1.5,
                CashAmount = -300,
                Date = date,
                TransactionType = TransactionType.Transaction
            };
            _transactionsRepositoryMock.Setup(x => x.GetCashAmountForUser(1)).Returns(1000);
            _transactionsRepositoryMock.Setup(x => x.AddTransaction(transaction));
            _mapperMock.Setup(x => x.Map<UserDto, User>(It.Is<UserDto>(u => u.Id == userDto.Id
                                                                            && u.Login == userDto.Login
                                                                            && u.Name == userDto.Name
                                                                            && u.Password == userDto.Password)))
                .Returns(user);

            var transactionService = new TransactionService(_transactionsRepositoryMock.Object, _mapperMock.Object);
            //Act
            var result = transactionService.BuyCurrency(transactionInputData);

            //Assert
            Assert.IsTrue(result);
            _transactionsRepositoryMock.Verify(x => x.AddTransaction(It.Is<Transaction>(t => t.Id == transaction.Id
                                                                                             && t.User == transaction.User
                                                                                             && t.ExchangeRate == transaction.ExchangeRate
                                                                                             && t.CurrencyCode == transaction.CurrencyCode
                                                                                             && t.Quantity == transaction.Quantity
                                                                                             && t.CashAmount == transaction.CashAmount
                                                                                             && t.Date == transaction.Date
                                                                                             && t.TransactionType == transaction.TransactionType)), Times.Once);
        }

        [Test]
        public void BuyCurrency_NotEnoughtFunds_False()
        {
            //Arrange
            var date = DateTime.Now;

            var userDto = new UserDto
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transactionInputData = new TransactionInputData
            {
                User = userDto,
                CurrencyCode = "BTC",
                Date = date,
                Quantity = 1.5,
                ExchangeRate = 200
            };
            var user = new User
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transaction = new Transaction
            {
                User = user,
                ExchangeRate = 200,
                CurrencyCode = "BTC",
                Quantity = 1.5,
                CashAmount = -300,
                Date = date,
                TransactionType = TransactionType.Transaction
            };
            _transactionsRepositoryMock.Setup(x => x.GetCashAmountForUser(1)).Returns(5);
            _transactionsRepositoryMock.Setup(x => x.AddTransaction(transaction));
            _mapperMock.Setup(x => x.Map<UserDto, User>(It.Is<UserDto>(u => u.Id == userDto.Id
                                                                            && u.Login == userDto.Login
                                                                            && u.Name == userDto.Name
                                                                            && u.Password == userDto.Password)))
                .Returns(user);

            var transactionService = new TransactionService(_transactionsRepositoryMock.Object, _mapperMock.Object);
            //Act
            var result = transactionService.BuyCurrency(transactionInputData);

            //Assert
            Assert.IsFalse(result);
            _transactionsRepositoryMock.Verify(x => x.AddTransaction(It.Is<Transaction>(t => t.Id == transaction.Id
                                                                                             && t.User == transaction.User
                                                                                             && t.ExchangeRate == transaction.ExchangeRate
                                                                                             && t.CurrencyCode == transaction.CurrencyCode
                                                                                             && t.Quantity == transaction.Quantity
                                                                                             && t.CashAmount == transaction.CashAmount
                                                                                             && t.Date == transaction.Date
                                                                                             && t.TransactionType == transaction.TransactionType)), Times.Never);
        }

        [Test]
        public void SellCurrency_ValidData_Success()
        {
            //Arrange
            var date = DateTime.Now;

            var userDto = new UserDto
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transactionInputData = new TransactionInputData
            {
                User = userDto,
                CurrencyCode = "BTC",
                Date = date,
                Quantity = 1.5,
                ExchangeRate = 200
            };
            var user = new User
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transaction = new Transaction
            {
                User = user,
                ExchangeRate = 200,
                CurrencyCode = "BTC",
                Quantity = -1.5,
                CashAmount = 300,
                Date = date,
                TransactionType = TransactionType.Transaction
            };
            _transactionsRepositoryMock.Setup(x => x.GetUsersOwnedCurrencyAmount(1, "BTC")).Returns(10);
            _transactionsRepositoryMock.Setup(x => x.AddTransaction(transaction));
            _mapperMock.Setup(x => x.Map<UserDto, User>(It.Is<UserDto>(u => u.Id == userDto.Id
                                                                            && u.Login == userDto.Login
                                                                            && u.Name == userDto.Name
                                                                            && u.Password == userDto.Password)))
                .Returns(user);

            var transactionService = new TransactionService(_transactionsRepositoryMock.Object, _mapperMock.Object);
            //Act
            var result = transactionService.SellCurrency(transactionInputData);

            //Assert
            Assert.IsTrue(result);
            _transactionsRepositoryMock.Verify(x => x.AddTransaction(It.Is<Transaction>(t => t.Id == transaction.Id
                                                                                             && t.User == transaction.User
                                                                                             && t.ExchangeRate == transaction.ExchangeRate
                                                                                             && t.CurrencyCode == transaction.CurrencyCode
                                                                                             && t.Quantity == transaction.Quantity
                                                                                             && t.CashAmount == transaction.CashAmount
                                                                                             && t.Date == transaction.Date
                                                                                             && t.TransactionType == transaction.TransactionType)), Times.Once);
        }

        [Test]
        public void SellCurrency_NotEnoughtFunds_False()
        {
            //Arrange
            var date = DateTime.Now;

            var userDto = new UserDto
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transactionInputData = new TransactionInputData
            {
                User = userDto,
                CurrencyCode = "BTC",
                Date = date,
                Quantity = 1.5,
                ExchangeRate = 200
            };
            var user = new User
            {
                Id = 1,
                Name = "test",
                Login = "testLogin",
                Password = "testPass"
            };
            var transaction = new Transaction
            {
                User = user,
                ExchangeRate = 200,
                CurrencyCode = "BTC",
                Quantity = -1.5,
                CashAmount = 300,
                Date = date,
                TransactionType = TransactionType.Transaction
            };
            _transactionsRepositoryMock.Setup(x => x.GetUsersOwnedCurrencyAmount(1,"BTC")).Returns(1);
            _transactionsRepositoryMock.Setup(x => x.AddTransaction(transaction));
            _mapperMock.Setup(x => x.Map<UserDto, User>(It.Is<UserDto>(u => u.Id == userDto.Id
                                                                            && u.Login == userDto.Login
                                                                            && u.Name == userDto.Name
                                                                            && u.Password == userDto.Password)))
                .Returns(user);

            var transactionService = new TransactionService(_transactionsRepositoryMock.Object, _mapperMock.Object);
            //Act
            var result = transactionService.SellCurrency(transactionInputData);

            //Assert
            Assert.IsFalse(result);
            _transactionsRepositoryMock.Verify(x => x.AddTransaction(It.Is<Transaction>(t => t.Id == transaction.Id
                                                                                             && t.User == transaction.User
                                                                                             && t.ExchangeRate == transaction.ExchangeRate
                                                                                             && t.CurrencyCode == transaction.CurrencyCode
                                                                                             && t.Quantity == transaction.Quantity
                                                                                             && t.CashAmount == transaction.CashAmount
                                                                                             && t.Date == transaction.Date
                                                                                             && t.TransactionType == transaction.TransactionType)), Times.Never);
        }
    }
}
