﻿using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using NUnit.Framework;
using StageTwoHomework.BusinessLayer.CurrencyProviders;
using Assert = NUnit.Framework.Assert;

namespace StageTwoHomework.BusinessLayer.Tests
{
    [TestFixture]
    public class ExchangeRateGeneratorTests
    {
        [Test]
        public void RunGenerator_CurrencyListNull_OnNewCurrienciesGeneratedNotCalled()
        {
            //Assert
            var wasCalled = false;
            var generator = new ExchangeRateGenerator();

            //Act
            generator.OnNewCurrienciesProvided += (o, e) => wasCalled = true;
            generator.StartProvider(null);//(new List<CurrencyDefinition>());
            Thread.Sleep(2000);

            //Assert
            Assert.IsFalse(wasCalled);
        }

        [Test]
        public void RunGenerator_DeffinitionList_ValidArgsOnNewCurrienciesGeneratedCalled()
        {
            //Assert
            var list = new List<CurrencyDefinition> { new CurrencyDefinition
            {
                Name = "Bitcoin",
                Code = "BTC",
                MaxRate = 40000,
                MinRate = 10000
            }, new CurrencyDefinition
            {
                Name = "Etherum",
                Code = "ETH",
                MaxRate = 20000,
                MinRate = 5000
            }};

            var generator = new ExchangeRateGenerator();

            //Act
            CurrencyRateGenerateEventArgs result = null;
            generator.StartProvider(list);
            generator.OnNewCurrienciesProvided += (sender, e) =>
            {
                result = e;
            };
            var refreshtime = int.Parse(ConfigurationManager.AppSettings["RefreshTime"]);
            Thread.Sleep(refreshtime * 2 );

            //Assert
            Assert.AreEqual(list[0].Name, result.Currencies[0].Name);
            Assert.AreEqual(list[0].Code, result.Currencies[0].Code);
            Assert.IsTrue(result.Currencies[0].ExchangeRate >= list[0].MinRate && result.Currencies[0].ExchangeRate <= list[0].MaxRate);
            Assert.AreEqual(list[1].Name, result.Currencies[1].Name);
            Assert.AreEqual(list[1].Code, result.Currencies[1].Code);
            Assert.IsTrue(result.Currencies[1].ExchangeRate >= list[1].MinRate && result.Currencies[1].ExchangeRate <= list[1].MaxRate);
        }
    }
}
