﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Moq;
using NUnit.Framework;
using StageTwoHomework.BusinessLayer.BusinessModels.Dtos;
using StageTwoHomework.BusinessLayer.Services;
using StageTwoHomework.DataLayer.Models;
using StageTwoHomework.DataLayer.Repositories.Interfaces;

namespace StageTwoHomework.BusinessLayer.Tests
{
    [TestFixture]
    public class UserServiceTests
    {
        private readonly Mock<IUserRepository> _userRepositoryMock = new Mock<IUserRepository>();
        private readonly Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private readonly Mock<ITransactionsRepository> _transactionsRepositoryMock = new Mock<ITransactionsRepository>();

        [Test]
        public void RegisterUserTest_ValidData_ValidUserId()
        {
            //Arrange
            var userDto = new UserDto
            {
                Login = "TestLogin",
                Name = "TestName",
                Password = "password"
            };
            var user = new User
            {
                Login = "TestLogin",
                Name = "TestName",
                Password = "password"
            };

            _userRepositoryMock.Setup(x => x.RegiesterUser(It.Is<User>(u => u.Login == userDto.Login
                                                                           && u.Name == userDto.Name
                                                                           && u.Password == userDto.Password)))
                  .Returns(10);

            _mapperMock.Setup(x => x.Map<UserDto, User>(It.Is<UserDto>(u => u.Login == userDto.Login
                                                                       && u.Name == userDto.Name
                                                                       && u.Password == userDto.Password)))
                .Returns(user);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object, _transactionsRepositoryMock.Object);

            //Act
            var result = userService.RegisterUser(userDto);

            //Assert
            Assert.AreEqual(10, result);
        }

        [Test]
        public void RegisterUserTest_Null_Zero()
        {
            //Arrange
            UserDto userDto = null;
            User user = null;


            _userRepositoryMock.Setup(x => x.RegiesterUser(user)).Returns(0);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object, _transactionsRepositoryMock.Object);

            //Act
            var result = userService.RegisterUser(userDto);

            //Assert
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetUserByLogin_ValidLogin_ValidUser()
        {
            //Arrange
            var login = "TestUser";
            var user = new User
            {
                Id = 5,
                Login = "TestUser",
                Name = "TestName",
                Password = "MyPassword"
            };
            var userDto = new UserDto
            {
                Id = 5,
                Login = "TestUser",
                Name = "TestName",
                Password = "MyPassword"
            };

            _userRepositoryMock.Setup(x => x.GetUserByLogin(login))
                .Returns(user);
            _mapperMock.Setup(x => x.Map<User, UserDto>(It.Is<User>(u => u.Login == user.Login
                                                                     && u.Name == user.Name
                                                                     && u.Password == user.Password)))
                                                                     .Returns(userDto);


            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object, _transactionsRepositoryMock.Object);

            //Act
            var result = userService.GetUserByLogin(login);

            //Assert

            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(user.Login, result.Login);
            Assert.AreEqual(user.Name, result.Name);
            Assert.AreEqual(user.Password, result.Password);
        }
        [Test]
        public void GetUserByLogin_NotExitsLogin_Null()
        {
            //Arrange
            var login = "TestUser";
            User user = null;


            _userRepositoryMock.Setup(x => x.GetUserByLogin(login)).Returns(user);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object, _transactionsRepositoryMock.Object);

            //Act
            var result = userService.GetUserByLogin(login);

            //Assert

            Assert.IsNull(result);

        }

        [Test]
        public void GetUserTransactionHistory_ValidId_ValidList()
        {
            //Arrange
            var userId = 5;
            var date = DateTime.Now;
            var transaction = new Transaction
            {
                Id = 1,
                ExchangeRate = 10,
                User = new User { Id = 5, Name = "test", Login = "test", Password = "test" },
                CurrencyCode = "BTC",
                TransactionType = TransactionType.Transaction,
                Date = date,
                CashAmount = 100,
                Quantity = 50
            };
            var transactionDto = new TransactionDto
            {
                Id = 1,
                ExchangeRate = 10,
                User = new UserDto { Id = 5, Name = "test", Login = "test", Password = "test" },
                CurrencyCode = "BTC",
                TransactionType = TransactionDtoType.Transaction,
                Date = date,
                CashAmount = 100,
                Quantity = 50
            };
            var transactionList = new List<Transaction>
            {
                transaction
            };

            _transactionsRepositoryMock.Setup(x => x.AllTransactionsForUser(userId))
                .Returns(transactionList);
            _mapperMock.Setup(x => x.Map<Transaction, TransactionDto>(It.Is<Transaction>(t => t == transaction)))
                .Returns(transactionDto);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object, _transactionsRepositoryMock.Object);
            //ACt

            var result = userService.GetUserTransactionHistory(userId);
            //Assert

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(transactionDto.Id, result[0].Id);
            Assert.AreEqual(transactionDto.CashAmount, result[0].CashAmount);
            Assert.AreEqual(transactionDto.CurrencyCode, result[0].CurrencyCode);
            Assert.AreEqual(transactionDto.Date, result[0].Date);
            Assert.AreEqual(transactionDto.ExchangeRate, result[0].ExchangeRate);
            Assert.AreEqual(transactionDto.Quantity, result[0].Quantity);
            Assert.AreEqual(transactionDto.TransactionType, result[0].TransactionType);
            Assert.AreEqual(transactionDto.User.Id, result[0].User.Id);
        }

        [Test]
        public void GetUserTransactionHistory_InvalidId_EmptyList()
        {
            //Arrange
            var userId = 5;

            var transactionList = new List<Transaction>();

            _transactionsRepositoryMock.Setup(x => x.AllTransactionsForUser(userId))
                .Returns(transactionList);
            _mapperMock.Setup(x => x.Map<Transaction, TransactionDto>(null)).Returns((TransactionDto)null);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object, _transactionsRepositoryMock.Object);
            //ACt

            var result = userService.GetUserTransactionHistory(userId);
            //Assert

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void GetUserFunds_UserId_ExpectedFunds()
        {
            //Arrange
            var userId = 5;
            var expectedFunds = 111.11;

            _transactionsRepositoryMock.Setup(x => x.GetCashAmountForUser(userId)).Returns(expectedFunds);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object, _transactionsRepositoryMock.Object);

            //ACt
            var result = userService.GetUserFunds(userId);

            //Assert  
            Assert.AreEqual(expectedFunds, result);
        }

        [Test]
        public void GetUsersCurrencyAmount_ValidInput_expectedCurrencyAmount()
        {
            //Arrange
            var userId = 5;
            var currencyCode = "BTC";
            var expectedCurrencyAmount = 99.99;

            _transactionsRepositoryMock.Setup(x => x.GetUsersOwnedCurrencyAmount(userId, currencyCode)).Returns(expectedCurrencyAmount);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object, _transactionsRepositoryMock.Object);

            //ACt
            var result = userService.GetUsersCurrencyAmount(userId, currencyCode);

            //Assert  
            Assert.AreEqual(expectedCurrencyAmount, result);
        }
    }
}
